# Cài đặt:

- Cài NodeJS từ: https://nodejs.org/en/download/
- Download PhantomJS 1.9 từ: https://bitbucket.org/ariya/phantomjs/downloads
- Download CasperJS(bản 1.1) từ: http://casperjs.org/
- Giải nén phamtonjs vào C:\phantomjs\
- Giải nén casperjs vào C:\casperjs\
- Thêm đường dẫn sau vào system path (control panel -> system -> advance system settings -> environment variables -> Path -> Edit)
      ;C:\Program Files (x86)\nodejs;C:\casperjs\bin;C:\phantomjs\bin
- Cài đặt bower:
  - vào cmd
  - gõ: npm install -g bower
- vào thư mục: casperCrawl
- tạo thư mục:
 - output/data
 - output/log

#Kiểm tra:

- Vào cmd gõ: casperjs, không hiện thông báo lỗi là OK.

#Cài thư viện:

- vào cmd:
- vào thư mục: casperCrawl
- gõ: npm install //cài thư viện dùng cho script main.js
- gõ: bower install //cài thư viện nhúng vào `cửa sổ ảo`

#Crawl:

- gõ: casperjs --web-security=no --ssl-protocol=tlsv1 main.js

