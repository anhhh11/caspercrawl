/**
 * Created by anhhh11 on 10/31/2015.
 */

/**
 * Có 2 môi trường:
 *  - Bên ngoài: là đoạn script đang chạy này
 *  - Bên trong: là môi trường bên trong cửa sổ ảo
 * Thư viện sử dụng:
 *  - Lodash: thư viện thay tác trên mảng
 *  - jQuery: thao tác trên DOM, CSS...
 */
var casper = require('casper').create({ // config
        clientScripts: ['bower_components/jquery/dist/jquery.js', //với mỗi `cửa sổ ảo` nạp thư viện jquery
                        'bower_components/lodash/lodash.js'],   // lodash sử dụng bên trong
        logLevel: "debug", // in tất cả log
        verbose: true, // ra màn hình
        pageSettings: {
            // đảm bảo hiển thị trên `cửa sổ ảo` giống hệt trên browser thực
            // do có 1 số web xét userAgent để xác định cách hiển thị
            userAgent: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36"
        }
    }),
    fs = require('fs'), //http://phantomjs.org/api/fs/
    _ = require('lodash'),
    md5 = require('md5');
    async = require('async'); // lodash sử dụng bên ngoài
//--------LOG ERROR------------//
var writeError = dataWriter('./output/error/google_com_q_casper',1,true,'.log');
var errorLogger = function(type){
    var screenErrorTemplate = _.template('[<%= date %>][<%= type %>][<%= hashed_url %>][URL: <%= url %>][Error: <%= error %>]'),
        captureErrorFileName = _.template('./output/error/<%= type %>_<%= hashed_url %>_<%= fixed_date %>.png'),
        writeErrorTemplate = _.template('[<%= date %>][<%= type %>][<%= hashed_url %>][URL: <%= url %>][Error: <%= error %>]');
    //ghi log ra màn hình + chụp hình + ghi log
    return function(){
        var now = new Date().toJSON(),
            url = casper.getCurrentUrl();
        var errorObject = {
            type: type,
            url: url,
            date: now,
            fixed_date: now.replace(/^\\.+/g, "").replace(/[\\\\/:*?\"<>|]/g, "_"),
            hashed_url: md5(url),
            error: JSON.stringify(arguments)
        };
        // Trong `cửa sổ mới` dùng __utils__.log thay cho console.log nếu muốn debug
        //API chi tiết: http://docs.casperjs.org/en/latest/modules/clientutils.html);
        casper.echo(screenErrorTemplate(errorObject));
        casper.capture(captureErrorFileName(errorObject));
        writeError(writeErrorTemplate(errorObject));
    }
};
var logTimeout = errorLogger('TIMEOUT').bind(casper);
casper.on('complete.error',errorLogger('complete.error')); //Emitted when a complete callback has errored.
casper.on('error',errorLogger('error')); //when an error hasn’t been explicitly caught within the CasperJS/PhantomJS environment.
casper.on('page.error',errorLogger('error')); //when retrieved page leaves a Javascript error uncaught:
casper.on('load.failed',errorLogger('load.failed')); //when PhantomJS’ WebPage.onLoadFinished event callback has been called and failed.
casper.on('resource.error',errorLogger('resource.error')); //requested resource fails to load properly.
casper.on('step.error',errorLogger('step.error'));//when a step function has errored.
casper.on('step.timeout',errorLogger('step.timeout')); //when a navigation step has timed out.
casper.on('waitFor.timeout',errorLogger('waitFor.timeout')); //when the execution time of a Casper.wait*() operation has exceeded the value of timeout.
//casper.on('resource.received', errorLogger('resource.received'));
//--------END LOG ERROR------------//

var links = [],
    title;

function startEndPart(from,to,at,partInterval){
    //var nrPart = Math.ceil((to - from) / partInterval);
    var atPart = Math.floor((at - from) / partInterval);
    var startPart = from + atPart * partInterval;
    return [startPart,Math.min(startPart + partInterval,to)];
}
/**
 * getNowDateStringRoundByMinutes
 * @param byMinutes
 * @returns {string} `day`_`month`_`year`_`hour`_`from minute`_`to minute`
 */
function getNowDateStringRoundByMinutes(byMinutes){
    var now = new Date();
    if(!byMinutes){
        byMinutes = 60;
    }
    var fromToMinute = startEndPart(0,60,now.getMinutes(),byMinutes);
    return now.getDay() + '_' + now.getMonth() + '_' + now.getFullYear() + '_' + now.getHours() + '_' + fromToMinute[0] + '_' + fromToMinute[1];
}
function dataWriter(filePath,intervalInMinutes,appendEol,ext){
    return function writeData(data) {
        var writtenData = JSON.stringify(data);
        fs.write(
            filePath + '_' + getNowDateStringRoundByMinutes(intervalInMinutes) +  ext,
            appendEol ? writtenData + '\n' : writtenData,
            'a');
    }
}
var writeData = dataWriter('./output/data/google_com_q_casper',1,true,'.json');
casper.start('http://google.com/', function () {
    //Điền form có selector: form[action="/search"]
    //điền vào trường [name="q"] giá trị casper
    //và submit sau khi điền
    this.fill('form[action="/search"]',{q: 'casperjs'},true); // Xem API ở: http://docs.casperjs.org/en/latest/modules/index.html
});
var getLinks = function () { // function bên trong this.evaluate chạy bên trong `cửa sổ ảo`
    // Ở `cửa sổ ảo` đã nạp jQuery và lodash lúc config ở: clientScripts
    var $links = $('h3.r a');
    return _($links)
        .map(function (link) {
            return $(link).attr('href');
        })
        .value();
};
// Do google nạp không đồng bộ(async) nên cần có thẻ cắm mốc xác định là đã nạp xong
// Các trang không nạp bằng JS thì không cần dùng waitForSelector
casper.waitForSelector('h3.r a', function () {
    links = this.evaluate(getLinks);
    // chỉ có 1 các tương tác giữa 2 môi trường là sử dụng hàm this.evaluate, và return giá trị về
    writeData(links);
    title = this.evaluate(function () { // môi trường bên trong
        return $(document).find("title").text();
    });
},function(){},5000); // timeout thì vẫn tiếp tục

casper.then(function () {
    //Lấy tiếp 10 trang
    // Chi tiết API async
    async.timesSeries(100,function(n,next){
        if(casper.exists('#pnnext')){ // Xem có nút next không
            casper.log(_.template('Page <%= page %>')({
                page: n + 1
            }),'info');
            // Bấm next
            casper.thenClick('#pnnext',function(){
                // Bắt buộc dùng `waitForSelectorTextChange` thay cho `waitForSelector`
                // nếu không thì nó có thể trả về $('h3.r a') của trang trước đó
                // do chưa kịp nạp lên DOM
                casper.waitForSelectorTextChange('h3.r a', function () {
                    var newLinks = this.evaluate(getLinks); // Lấy link mới
                    //casper.log(JSON.stringify(newLinks,null,'\t'),'info');
                    writeData(newLinks);
                    //links = links.concat(newLinks); // Ghép vào danh sách link trước đo
                    next(); // Gọi next để báo hiệu cho async chạy tiếp sang lần mới.
                }, async.compose(next),5000); // timeout thì vẫn tiếp tục
            });
        }
    });
});

casper.run(function () {
    this.echo(title);
});
casper.on('run.complete', function() {
    this.echo('Done');
    this.exit();
});



